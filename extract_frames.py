
import os, sys, cv2

video_filenames = os.listdir(sys.path[0] + '/inputs')

for video_file_name in video_filenames:

    print video_file_name
    file_name_no_extension = video_file_name.split(".")[0]
    output_dir_for_this_file = 'output/' + file_name_no_extension
    os.makedirs(output_dir_for_this_file)

    video = cv2.VideoCapture(sys.path[0] + '/inputs/' + video_file_name)
    okay = True
    frame_number = 1
    while okay:
        okay, frame = video.read()
        if okay:
            cv2.imwrite(os.path.join(output_dir_for_this_file,
                                     file_name_no_extension + '_' + str(frame_number).zfill(4) + ".jpg"), frame.copy())
            frame_number += 1